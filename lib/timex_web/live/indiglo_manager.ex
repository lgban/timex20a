defmodule TimexWeb.IndigloManager do
    use GenServer

    def init(ui) do
        :gproc.reg({:p, :l, :ui_event})
        {:ok, %{ui_pid: ui, st: :indiglo_off}}
    end

    def handle_info(:"top-right", %{ui_pid: ui, st: :indiglo_off} = state) do
        GenServer.cast(ui, :set_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_on)}
    end

    def handle_info(:"top-right", %{ui_pid: ui, st: :indiglo_on} = state) do
        Process.send_after(self(), :tick, 2000)
        {:noreply, state |> Map.put(:st, :waiting)}
    end

    def handle_info(:tick, %{ui_pid: ui, st: :waiting} = state) do
        GenServer.cast(ui, :unset_indiglo)
        {:noreply, state |> Map.put(:st, :indiglo_off)}
    end

    def handle_info(event, state), do: {:noreply, state}
end